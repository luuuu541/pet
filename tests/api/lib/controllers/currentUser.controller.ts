import {ApiRequest} from '../request';

export class currentUserController {
    async getCurrentUser() {
        const response = await new ApiRequest()
            .prefixUrl('http://tasque.lol/api/')
            .method('GET')
            .url(`Users/fromToken`)
            .send();
        return response;
    }
}