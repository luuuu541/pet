import {ApiRequest} from '../request';

export class fromTokenController {
    async getFromToken() {
        const response = await new ApiRequest()
            .prefixUrl('http://tasque.lol/api/')
            .method('GET')
            .url(`Users/fromToken`)
            .send();
        return response;
    }
}