import {ApiRequest} from '../request';

export class usersController {
    async getUsers() {
        const response = await new ApiRequest()
            .prefixUrl('http://tasque.lol/api/')
            .method('GET')
            .url(`Users`)
            .send();
        return response;

        
    }

}