import {ApiRequest} from '../request';

export class updateUserController {
    async putUpdateUser() {
        const response = await new ApiRequest()
            .prefixUrl('http://tasque.lol/api/')
            .method('POST')
            .url(`Users`)
            .body({
                "id": 111111,
                "email": "123456@gmail.com",
                "userName": "1234"
              })
            .send();
        return response;
    }
}